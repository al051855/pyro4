
import Pyro4
import tkinter as tk
import tkinter.messagebox
import time
import threading

@Pyro4.expose
class VotingClient:
    def __init__(self):
        host = input("Ingresar ip del name server: ")
        ns = Pyro4.locateNS(host=host)
        url = ns.lookup("Voting")
        self.server = Pyro4.Proxy(url)

    def vote(self, candidate, votante):
        votoReg = self.server.vote(candidate, votante)
        if votoReg:
            tkinter.messagebox.showinfo("Voto registrado", f"Su voto por {candidate} a sido registrado con exito")
        else:
            tkinter.messagebox.showerror("Voto no registrado", "No se a registrado su voto, esto puede ser debido a que usted ya habia votado antes")

    def results(self):
        return self.server.results()



class VotingUI(tk.Frame):
    def __init__(self, master, client):
        super().__init__(master)
        self.client = client
        self.create_widgets()
        self.pack()
        self.update_time = 0.25
        threading.Thread(target=self.temporizador).start()


    def create_widgets(self):
        self.candidates = list(self.client.results().keys())
        tk.Label(self, text="Clave de elector:").pack()
        self.votante_clave_elector = tk.Entry(self)
        self.votante_clave_elector.pack()
        self.choice = tk.StringVar(value=self.candidates[0])
        for candidate in self.candidates:
            tk.Radiobutton(self, text=candidate, variable=self.choice, value=candidate).pack(side='top', anchor='w')
        tk.Button(self, text='Votar', command=self.vote).pack(side='top')
        self.results = tk.Label(self, text='')
        self.results.pack(side='top')
        self.update_results_data()

    def temporizador(self):
        while True:
            self.update_results_data()
            time.sleep(self.update_time)

    def update_results_data(self):
        results = self.client.results()
        self.results.configure(text=f"Resultados: {', '.join([f'{candidate}: {votes}' for candidate, votes in results.items()])}")

    def vote(self):
        self.client.vote(self.choice.get(), self.votante_clave_elector.get())
        self.update_results_data()



def start_voting_ui(client):
    root = tk.Tk()
    VotingUI(root, client)
    root.mainloop()

def start_client():
    client = VotingClient()
    start_voting_ui(client)

if __name__ == '__main__':
    start_client()
