
import Pyro4
import Pyro4.naming


@Pyro4.expose
class VotingServer:
    def __init__(self, candidates, votantes):
        self.candidates = candidates
        self.votantes = votantes
        self.votes = {candidate: 0 for candidate in candidates}


    def start(self):
        host = input("Ingresar ip del name server, sera la misma que la ip del servidor de votacion: ")
        daemon = Pyro4.Daemon(host=host)
        url = daemon.register(self)
        ns = Pyro4.locateNS(host=host)
        ns.register("Voting", url)
        print(ns)
        print(f'Servidor en linea', f'URI={url}', sep="\n")
        daemon.requestLoop()

    
    def vote(self, candidate, votante):
        if votante not in votantes:
            votantes.append(votante)
            if candidate in self.candidates:
                self.votes[candidate] += 1
                return True
        return False
    

    def results(self):
        return self.votes



if __name__ == '__main__':
    candidates = ['PRI', 'PAN', 'PRD', 'PT', 'Verde']
    votantes = []
    server = VotingServer(candidates, votantes)
    server.start()
